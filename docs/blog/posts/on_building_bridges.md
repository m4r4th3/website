---
slug: on-building-bridges
date: 2020-12-11T00:00:00
authors:
  - vanous
categories:
  - About
---

# On building safe bridges

Cool gadgets give us some really great possibilities, like tracking our
activities, controlling music and smart home devices, receiving notifications
and more. But in our strive for better, faster, lighter, smarter, cheaper... we
often forget the down side of our endeavors. Apps providing connections to our
devices do receive all our movement information, including precise location. We
have written about it [previously](why-gadgetbridge.md), in 2017. But maybe
you do not care about the details of your whereabouts being shared and only
care about your communication. And your communication with friends and family
is surely safe with secure apps that have become widely popular, right? But
unfortunately, every notification that is forwarded to your smart bracelet even
though you received it from some very secure, end to end encrypted app like
Matrix or Signal, has been seen by the bracelet's companion app. These are
closed source applications provided mostly by the vendor of the gadget and it
is only up to the vendor to decide if they send these conversations to their
servers for further processing. It is now up to us to decide if we want to give
them fully detailed view into our lives.
<!-- more -->

So this is where Gadgetbridge comes in, of course, but it is important to
realize that it takes a substantial effort to keep things running correctly. To
ensure stable Bluetooth connection. To provide reliable firmware update system.
To collect and display daily activities in a user friendly way. To keep up with
firmware updates of all those supported watches and make sure stuff does not
break (too much). And also, to provide a safe bridge between the smart bracelet
and the phone, so notifications are not observed by other applications. For
these very reason, Gadgetbridge does not have any network access, in fact it
does not have network permission, so the Android system does not allow it to
have any network communication at all. And this is good - good for the safety,
good for the clear mind. Nice, clean, safe bridge between your phone and your
smart bracelet's app. Thanks to all currently active but also to all previously
very active developers for all the hard work!

But we must have even higher goals. People would like to not only collect their
own data, but they also want to use it, look at it, analyze it or share it - if
they want. So we should not only collect all the steps, activities hear
rates... we must also provide a way to export it to be able to use it on day to
day bases, easily. To share for example GPS information recorded on some
watches, Gadgetbridge does allow exporting GPX files of recorded sports
activities as files or as intents that other applications can use. Gadgetbridge
can also export full database of all collected stuff (steps, heart rate and so
on), together with all user preferences. But we must ensure that there are also
apps that can receive our data - we should build more connections between our
apps. Recently, the good folks at [Open
Tracks](https://f-droid.org/packages/de.dennisguse.opentracks/) improved their
GPX input intent listener, providing yet another app that can directly receive
recorded GPS track shared by Gadgetbridge. They also worked hard to navigate
through the confusing landscape of Bluetooth Low Energy communication to be
able to accept data from heart rate sensor, enabled on the bracelet via
Gadgetbridge settings. Same heart rate data can now also be used with the
[FitoTrack](https://f-droid.org/packages/de.tadris.fitness/). The
[InfiniTime](https://infinitime.io/) developers
[improved](https://infinitime.io/blog/2020-10/infinitime-0-9-0/) music controls
and firmware updates, making a nice connection to Gadgetbridge. And we all
should keep looking around for improving things in our small islands of app
created silos and connect them with nice bridges. I strongly advice you to be
creative in your thinking, talk to the developers of your favorite apps and
propose ways for apps to keep talking to each other. No <strike>\[hu\]man</strike> app should be
an island.
