---
title: Pairing
icon: material/link-variant
---

# Pairing

"Pairing" means establishing a connection between two Bluetooth devices. It can be a complex process and the type of pairing can determine what possibilities are given to the connected device during day-to-day operation.

Before you add the device to Gadgetbridge, it is recommended to unpair the gadget from your phone's Bluetooth and uninstall the vendor's app. If your gadget supports Bluetooth pairing then enable it, so when you add your gadget in Gadgetbridge, it will also be paired in the Android Bluetooth settings of your phone.

To connect a new gadget, press **:material-plus-box: Add** on the bottom-right corner of the home screen in Gadgetbridge. Then you will be presented with the "Device discovery" page.

## Device discovery

<div class="gb-step" markdown>
![](../../assets/static/screenshots/device_discovery.jpg){: height="600" style="height: 600px;" }
<div markdown>

Click "Start discovery" to search for gadgets nearby, and wait until it finds your gadget.

If Gadgetbridge asks for a "key" next to the your gadget name, follow the authentication key section from the button below and continue here after retrieving and configuring your authentication key.

[:material-arrow-right: Authentication key](#authentication-key){: .gb-button }

Otherwise, click your device name to pair with Gadgetbridge and if everything goes fine, the device will appear on the Gadgetbridge home screen. You can now start using your gadget with Gadgetbridge! If your gadget asks to connect it via the official app (and Gadgetbridge doesn't ask for a "key"), you can ignore the screen in the gadget, you can continue with only Gadgetbridge.

[:material-arrow-right: Configuring your gadget](../features/index.md){: .gb-button }

</div></div>

## Troubleshooting

In the **Discovery and Paring** options (available from Device discovery as well as from Settings) there are a few options that are related to pairing. If your scan never finishes and seems to cause Gadgetbridge to freeze and become non-responsive, check the "Disable new BLE scanning". You can also re-try the pairing in a place with less Bluetooth traffic.

## Authentication key

!!! warning "This section isn't for you if you are not asked for a key"
    You should follow this section only if Gadgetbridge explicitly asks for a key when pairing your gadget. (It shows as **KEY REQUIRED** on the discovery screen.)

Depending on which gadget you use, you might be asked for an "authentication key" instead. These gadgets communicate with an encrypted protocol, thus requiring both the gadget and Gadgetbridge to know a key for encrypting and decrypting the communication.

It is in most of these cases simply impossible to add your device to Gadgetbridge unless you provide a valid authentication key. Some devices (for example Fossil) can still be connected but support less features when no key or an invalid key was provided.

![](../../assets/static/screenshots/device_discovery_auth.jpg){: height="600" style="height: 600px;" }

As we don't know how proprietary gadgets work internally (proprietary firmware, obviously), we can't simply create the key for you, so you will usually need to extract it by pairing your gadget first with its official (vendor) application or a third-party app created specifically for this purpose.

After you successfully paired your gadget on the vendor app, you can use various methods to obtain the authentication key from the vendor's servers, then later configure it in Gadgetbridge.

If you have a Xiaomi or Amazfit gadget manufactured my Huami (uses Zepp Life / Zepp / Mi Fitness / Xiaomi Wear app) that requires an key, click on the button below to learn how to obtain the key:

[:material-arrow-right: Huami/Xiaomi server pairing](./huami-xiaomi-server.md){: .gb-button }

If you have a Fossil gadget that requires a key, click on the button below to learn how to obtain the key:

[:material-arrow-right: Fossil server pairing](./fossil-server.md){: .gb-button }

## Pairing unsupported gadgets

!!! warning "This section isn't for you if you are not advised to do that"
    Do not perform pairing unsupported gadgets without being advised.

If your gadget is not supported by Gadgetbridge, but you have a gadget that is  similar to one that is supported by Gadgetbridge, you can try pairing it with Gadgetbridge to see if it works as-is and let us know.

Do note that if the gadget requires special pairing key (like Huami gadgets), you must obtain it before pairing, see above.

Other requirements from above are still valid, if your gadget's official app is running, stop or uninstall it and unpair the gadget from Bluetooth menu, (if it is already paired).

<div class="gb-step" markdown>
![](../../assets/static/screenshots/device_discovery_options.jpg){: height="600" style="height: 600px;" }
<div markdown>

From "Discovery and Pairing options" (available from [Device Discovery](#device-discovery)), enable "Discover unsupported devices".

Next searches of nearby gadgets will also list unknown devices from now on.

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/device_discovery_test.jpg){: height="600" style="height: 600px;" }
<div markdown>

Press and hold on an "unknown" labeled gadget to show the "Add test device" menu. Now you can choose like which gadget you want Gadgetbridge to handle your device.

You should choose the gadget which is the most similar to yours (model name, operation logic and etc.) for the best compatibility.

</div></div>
