---
title: PebbleKit Compatibility
---

# PebbleKit Compatibility

Information about compatibility of Android apps that use PebbleKit features, for gadgets managed with Gadgetbridge.

## Confirmed working apps

* "Dashboard" on [:simple-googleplay: Google Play](https://play.google.com/store/apps/details?id=com.wordpress.ninedof.dashboard){: target="_blank" }
* "Meerun" on [:simple-googleplay: Google Play](https://play.google.com/store/apps/details?id=com.bwa.meerun){: target="_blank" }
* "Notification Center" on [:simple-googleplay: Google Play](https://play.google.com/store/apps/details?id=com.matejdro.pebblenotificationcenter){: target="_blank" }
* "PebbleTasker" on [:simple-googleplay: Google Play](https://play.google.com/store/apps/details?id=com.kodek.pebbletasker){: target="_blank" }
* "RunnerUp" on [:simple-fdroid: F-Droid](https://f-droid.org/tr/packages/org.runnerup.free/){: target="_blank" }
* "JayPS" on [:simple-github: GitHub](https://github.com/team-mount-ventoux/JayPS-AndroidApp){: target="_blank" }

## Apps with issues / workarounds

### Canvas for Pebble

Canvas for Pebble on [:simple-googleplay: Google Play.](https://play.google.com/store/apps/details?id=com.pennas.pebblecanvas){: target="_blank" }

Does not work. Complains about "Pebble not connected". See also {{ 130|issue }}. One can manually install the pbw from `/sdcard0/Android/data/com.pennas.pebblecanvas/files/`. Might work with Gadgetbridge > 0.7.2 works. Version 4.12 of Android app (with Pebble app version 4.8). Tested with Gadgetbridge 0.26.5

### Pebble Dialer

Works on Pebble Classic, might be broken or incomplete on Pebble Time, since it distinguishes between Aplite and Basalt (for caller pictures). Needs more testing.

### Push to Pebble

Push to Pebble on [:simple-googleplay: Google Play.](https://play.google.com/store/apps/details?id=com.mohammadag.pushtopebble){: target="_blank" }

Does not work as complains about "No Pebble Connected or Pebble app not installed". See also {{ 130|issue }}. Might work with Gadgetbridge > 0.7.2 though.

### Sleep as Android

Sleep as Android on [:simple-googleplay: Google Play.](https://play.google.com/store/apps/details?id=com.urbandroid.sleep){: target="_blank" }

Works, but needs to be set up with official Pebble app first:

1. Set up Pebble with Gadgetbridge normally.
2. Install Sleep as Android to Pebble [with firmware installer.](../../internals/features/installer.md#pebble){: target="_blank" }
2. Install official Pebble app to your phone. (only install, no need to open the app or set it up)
3. Open Sleep as Android on your phone.
4. Go to "Settings → Wearables → Use Wearables".
5. Select Pebble.
6. Start Sleep Tracking on the Android device. App should start on the Pebble. Make sure everything works correctly.
7. Uninstall official Pebble app.
