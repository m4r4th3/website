---
title: Features
icon: material/shape
---

# Features

After pairing your gadget, you can now manage it in Gadgetbridge.

<div class="gb-step" markdown>
![](../../assets/static/screenshots/home.jpg){: height="600" style="height: 600px;" }
<div markdown>

This is the home screen. There is a list of your gadgets, and under the gadget name, you can see your daily activity information at a glance.

Tap the **:material-cog: Settings** icon under your gadget name to access the "Device specific settings".

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/device_specific_settings_1.jpg){: height="600" style="height: 600px;" }
<div markdown>

You can configure your gadget's settings here. These options are different for every gadget, so the screenshot may differ from what you are seeing.

For more details of each feature, see the left navigation of this documentation.

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/home_device_menu.jpg){: height="600" style="height: 600px;" }
<div markdown>

You can also tap the **:material-dots-vertical: More** icon next to the gadget name on the home screen for additional options.

</div></div>

<div class="gb-step" markdown>
![](../../assets/static/screenshots/sidebar_settings.jpg){: height="600" style="height: 600px;" }
<div markdown>

To access the global/general settings of Gadgetbridge, tap the **:material-menu: Menu** icon or swipe from the left side to the right on the home screen to access the sidebar, then tap **:material-cog: Settings**.

</div></div>

{{ file_listing() }}
