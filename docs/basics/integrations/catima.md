---
title: Loyalty cards with Catima
---

# Loyalty cards with Catima

Gadgetbridge can sync loyalty cards stored in [Catima](https://catima.app/){: target="_blank" } with supported gadgets that can store loyalty & membership cards.

Catima support was added with {{ 2953|pull }} for Zepp OS gadgets, and with {{ 3268|pull }} for Bangle.js. The cards are synced on-demand by clicking a button in the preferences. Gadgetbridge will query the gadget to check if it supports storing loyalty cards (for Zepp OS gadgets), and if not, you won't be able to see Catima option in Gadgetbridge.

There is no list of Zepp OS gadgets that supports storing loyalty cards, but apparently, gadgets with Zepp OS version from 2.0 onwards <sup>(verification needed)</sup> have this feature, some of them are:

* Amazfit Bip 5 <!-- https://www.amazfit.com/products/amazfit-bip-5 -->
* Amazfit GTR 4 <!-- https://codeberg.org/Freeyourgadget/Gadgetbridge/pulls/2953#issuecomment-1210073 -->
* Amazfit GTS 4 <!-- https://www.reddit.com/r/amazfit/comments/18qi7ra/anyone_else_having_trouble_adding_membership/ -->
* Amazfit Balance <!-- https://www.reddit.com/r/amazfit/comments/17qujzf/membership_cards/ -->

From Amazfit's [website](https://www.amazfit.com/products/amazfit-gtr-4){: target="_blank" } (under "Notes" section) for GTR 4, it might be the same for the other supported gadgets too:

> Due to the device's storage limit, only a maximum of 20 membership cards can be added to the same account. Supported codes include Code 128, Code 39, and QR codes.

!!! warning "Not to be confused with contactless payments"
    Note that loyalty cards are different from contactless payments, the former is just about noninterchangeable barcodes which is used for tickets, memberships and loyalties, thus can't be used for communicating & paying on point-of-sale payments. For the latter, both your gadget manufacturer and the bank needs to support paying from your gadget, which usually requires an business arrangement with the bank and integrate their proprietary code, such as in [Zepp Pay.](https://www.amazfit.com/pages/zepp-pay){: target="_blank" } So this is not supported by Gadgetbridge.