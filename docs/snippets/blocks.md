<!-- --8<-- [start:firmware_danger] -->

!!! danger "Danger"
    Installing firmwares **has the potential to brick your gadget**! That said, this has never happened to any of the developers, but remember **you're doing this at your own risk**. See [Install a firmware or watchface](../features/installer.md) to know how to install a firmware.

<!-- --8<-- [end:firmware_danger] -->
