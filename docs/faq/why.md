---
title: Why Gadgetbridge?
---

# Why Gadgetbridge?

You can always use your gadget's own vendor app, but there are advantages of Gadgetbridge over vendor apps:

* Vendor apps are not free (as-in freedom) and open source. You can't rely on them to protect your online rights, nor can you rely on them remaining available forever.
* Vendor apps have Internet permission, which allows them to transmit your data to the vendor's cloud service or even third parties. Gadgetbridge doesn't even have that permission, which makes it impossible to send any data to the internet.
* Vendor apps don't always allow you to use all features of your gadget, whereas Gadgetbridge lists all supported settings of your gadget. In some cases, Gadgetbridge may not support all features when compared to the vendor app (yet), but in other cases Gadgetbridge supports more than the official app.
* Vendor apps require you to create an online account. While some gadgets require this for online pairing key generation, an account is not required for daily usage like controlling your gadget and syncing with your phone.
* Gadgetbridge supports many different gadgets, allowing you to manage all of them in a single app, rather than installing a separate app for each gadget.
