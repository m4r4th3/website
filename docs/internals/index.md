---
title: Internals
icon: material/tools
---

# Internals

This section of the documentation contains technical information about inner communication protocols of gadgets, implementing a new gadget, and use of automation applications with Gadgetbridge - which are all mostly useful for developers.

## Quick links

<div class="gb-section" markdown>
:material-checkbox-multiple-marked-outline:
<div markdown>

Gadgetbridge wants to support all good and wide-spread gadgets. It is encouraged to work on the implementation of new gadgets. If you just want request support for a new gadget or if you have the knowledge yourself for implementing a new gadget, see [Supporting a new gadget](./topics/support.md).

</div></div>

<div class="gb-section" markdown>
:material-flash:
<div markdown>

If you want to call Gadgetbridge functions from your computer or from another Android application, see [Intents](./automations/intents.md), or if you want to perform any task when you wake up or sleep, see [Automation example](./automations/example.md) for example usage with Tasker or similar automation applications.

</div></div>

<div class="gb-section" markdown>
:material-package-variant-plus:
<div markdown>

Gadgetbridge comes with a built-in firmware installer to install firmware, apps, watchfaces and resource files in a few steps. To learn how, see [Install firmware & watchface](./features/installer.md).

</div></div>

<div class="gb-section" markdown>
:material-dns:
<div markdown>

If you are planning to switch to an another ROM or Android device, (or if you just want to reset to factory settings) then you may want to export your Gadgetbridge data. You can import it back again at any time without losing your activity history. See [Data management](./development/data-management.md#data-folder-location).

</div></div>
