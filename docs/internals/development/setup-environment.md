---
title: Setup the environment
---

# Setup the environment

!!! warning "Warning"
    Be aware that you cannot install your own built version (`mainDebug`) and F-Droid's at the same time due to the conflict in Pebble provider, but you can use the `mainNopebble` or `banglejsDebug` build types for that, see [Build flavors and types](#build-flavors-and-types). You can export and import your database, though.

## Choosing an IDE

### Android Studio

The core team uses Debian GNU/Linux + [Android Studio](https://developer.android.com/studio/index.html){: target="_blank" }, latest version. It is also available for Windows and Mac, but we never used that. You can use any Android device connected to USB to run your code straight from Android Studio on the device.

### Codium (Visual Studio Code)

This has limitations but can be a way to develop too. You need to install Codium (which is Visual Studio Code but without any telemetry, you can also go with Visual Studio Code itself, but be aware that it contains telemetry.).

Required plugins that must be installed:

* [Android by adelphes](https://open-vsx.org/extension/adelphes/android-dev-ext){: target="_blank" }, this provides the build/debug/logcat functions.
* [Language Support for Java(TM) by Red Hat](https://open-vsx.org/vscode/item?itemName=redhat.java){: target="_blank" }, provides the code navigation with <kbd>F12</kbd>.

[See here](https://www.linkedin.com/pulse/building-android-apps-using-microsofts-vs-code-instead-saamer-mansoor/){: target="_blank" } to create a Launch config. Make sure when opening a folder to open the actual root of the project, not the `./app` as suggested in the article.

* For debugging, open the Logcat console by <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>P</kbd> → Android: View Logcat.

## Installing dependencies

You need to install:

* A Java Development Kit (JDK)
* Git
* ADB (Android Debug Bridge, included in Android Studio)

On Ubuntu, you can use the following command:

```bash
sudo apt-get install openjdk-17-jdk git adb
```

### Docker image

There are also [docker images available](https://github.com/docker-android-sdk) with all required tools inside.

To start a container with USB connection to a phone, execute e.g.

```
host $ docker run -it --rm --device=/dev/bus --net=host -v ${PWD}:/src androidsdk/android-30:latest bash
container # cd /src
```

This mounts the current directory into the container under `/src` and then allows to build and install the results.

## Currently used SDK versions

_(as of October 2023)_

- `compileSdkVersion` is 33
- `minSdkVersion` is 21
- `targetSdkVersion` is 33

!!! question "The minSdkVersion is rather low?"
    Do not upgrade the SDK, including the `minSdkVersion`, we still would like to keep support for Android 5.0 (Lollipop). 
    
    Another problem with upgrading is that since we have a lot of deep integration into the Android system (to make sure that calls, notifications, media... can be controlled, displayed and so on from a Bluetooth tethered device), updating the SDK needs a lot of testing on many devices and Android versions, since every time we have bumped the SDK it caused (a lot of hidden) problems for us. 
    
    Also, we cannot downgrade the SDK anymore once it has been bumped.

!!! question "What about Kotlin or Dart & Flutter?"
    Kotlin could be nice but this project started long before Kotlin was added as
    Android supported alternative to the Java compiler. Rewrite is unfortunately
    not what we are planning on. Making APK larger by adding Kotlin for some parts
    is also not preferred.

    Flutter is not a great choice [from a software freedom perspective](https://github.com/flutter/flutter/issues/104219){: target="_blank" }.


## Getting the code

```bash
git clone https://codeberg.org/Freeyourgadget/Gadgetbridge.git
```

After you did that once, you can use `git pull` to get the newest Gadgetbridge code.

Alternatively you can use Android Studio to clone the Gadgetbridge repository.

## Build flavors and types

### Product Flavors

#### Main

There is the `main` flavor which is used for normal day to day coding, F-Droid releases, for Nightly releases and so on.  

#### Banglejs

Then there is the `banglejs` flavor. This particular custom flavor makes use of some extra features, together with added permission for networking. Do note that this permission is not enabled for our normal Gadgetbridge releases and is only available on the Bangle.js specific build, as the Bangle.js watch is built around their online application repository and it requires internet connectivity for proper function. 

We believe that this cooperation with the Bangle project is very useful for both sides and that communities around both Bangle.js and Gadgetbridge can benefit from this connection. 

We can already see some contributions that help to clarify for example requests for permissions, making Gadgetbridge more user friendly during the initial onboarding experience. Bangle implementation also got new communication intents to be able to share data from/to other Android apps.

### Build Types

There are several build types:

* `release` - for releases (F-Droid...).
* `debug` - for developing and local testing.
* `nightly` - for nightly release in our [own F-Droid repo](https://freeyourgadget.codeberg.page/fdroid/repo/). It cannot be installed if you already have either the Gadgetbridge or the Pebble app installed, due to a conflict in the Pebble provider.
* `nopebble` - for nightly release in our [own F-Droid repo](https://freeyourgadget.codeberg.page/fdroid/repo/). This version has the Pebble provider renamed to prevent conflicts, so some Pebble related integrations will not work, but it can be installed alongside existing Gadgetbridge installation. 

## Running tests

All tests:

```
./gradlew test
```

Running just one specific tests:

```
./gradlew :app:testBangleDebugUnitTest :app:testMainDebugUnitTest
```

Or run just one particular test class:

```
./gradlew :app:testMainDebugUnitTest --tests nodomain.freeyourgadget.gadgetbridge.test.FitProTests
```

## Building and installing the application

If you only want to compile the code, you can simply execute:

```
./gradlew assembleMainDebug
```

or install it:

```
./gradlew installMainDebug
```

Android Studio does all this automatically when you press the `Run` or `Debug` button, you may have to open the root directory of the repo for the configuration to be loaded.

<!-- TODO: image missing -->

---

## Next steps

Now, you can start exploring and building on Gadgetbridge code!

[:material-arrow-right: Project overview](./project-overview.md){: .gb-button }
