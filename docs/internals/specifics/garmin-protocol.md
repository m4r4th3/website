---
title: Garmin Protocol
---

# Garmin Protocol

There are (at least) two big families of garmin protocol: both are using BLE. The first protocol uses different characteristics to transfer different type of information and appears to be in use on older wearables: the aforementioned contribution made Gadgetbridge support this version of the protocol.

A newer protocol uses the same characteristics for every information transfer, by encapsulating this in "threads" (or handles, or conversations) whose identifiers are prepended to each message exchange. This version of the protocol is currently unsupported.

A third version of the protocol was also reported (see {{ 3063|issue }}) that apparently adds some kind of sequence between the thread/handle/conversation ID and the actual payload, while using the same characteristic.

The difference between the first protocol and the rest can be detected early by the presence of the specific characteristic used in newer protocols. The second and the third protocol might probably be chosen according to some early handshake values and the way some identifiers are chosen by the watch, so it should be possible to support all three generations of the protocols with relatively little effort.
