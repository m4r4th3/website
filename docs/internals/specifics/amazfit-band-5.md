---
title: Amazfit Band 5 Firmware Update
---

# Amazfit Band 5 Firmware Update

--8<-- "blocks.md:firmware_danger"

## Getting the firmware

Since we may not distribute the firmware, you have to find it elsewhere :( Amazfit Band 5 firmware updates were never part of Mi Fit/Zepp, but are only made available via OTA updates.

### Choosing the right firmware version

Device Name                      | HW Revisions                          | Codename | Default Language |
---------------------------------|---------------------------------------|----------|------------------|
Amazfit Band 5                  | V0.57.18.18                            | YORK | English          |

*It seems that the firmware for L and non L variant are unified*

## Installing the firmware

Copy the desired Amazfit Band 5 `.fw` and `.res` files to your Android device and open the `.fw` file from any file manager on that device. The Gadgetbridge firmware update activity should then be started and guide you through the installation process. Repeat with the `.res` file (you don't have to do this if the previous version you flashed had exactly the same `.res` version).

If you get the error "Element cannot be installed" or "The file format is not supported" try a different file manager. [Amaze](https://f-droid.org/en/packages/com.amaze.filemanager/){: target="_blank" } should work.

## Known firmware versions

### Amazfit Band 5

fw ver   | tested | known issues | res ver | fw-md5 | res-md5 
---------|--------|-------------------|---------|--------|---------
1.0.1.54 | yes    | ? | v104 | (preinstalled) | (preinstalled)
1.0.1.64 | yes    | ? | v106 | 5059569a309ce4447ead190f1d2ba778 | eb1c8065f5db04757f1a10754c0ccabf

## Custom font

You can flash through Gadgetbridge a custom `.ft` font file which will allow the device to display some emojis and accented characters.

You also need to enable "Use custom font" in your device specific settings.

As always, flash at **your own risk!**

Source: [Geekdoing](https://geekdoing.com/threads/mi-band-5-custom-fonts.1954/#post-33944){: target="_blank" }
