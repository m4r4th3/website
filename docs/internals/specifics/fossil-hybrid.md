---
title: Fossil Hybrid Firmware Update
---

# Fossil Hybrid Firmware Update

## Obtaining the required files

The Fossil/Skagen Hybrids functionality is split into two parts:

* **Firmware:** contains the most basic functionality of the watch, like hardware support, connectivity, workout app and notifications.
* **Watch apps:** these are separately installed apps (.wapp files) that contain additional functionality, like the watchface and apps like menu, weather, stopwatch, etc.

The Fossil/Skagen watches can currently be updated through Gadgetbridge, but it is required to obtain the firmware file and watch apps beforehand. These files are proprietary and owned by Fossil, so we cannot provide them to you without breaking copyright laws.

Since the files are not simply distributed inside the APKs of the official apps, there are only two relatively difficult ways to obtain them:

* Use mitmproxy to capture them when the official apps downloads them
* Use Bluetooth traffic sniffing to capture them when the official app sends them to the watch

Additionally, when the watch has been fully updated by the official app, it's possible to download/backup the watch apps from the watch using the App Manager in Gadgetbridge.

## Choosing the right firmware version

| Watch series                                   | Hardware revision |
| ---------------------------------------------- | ----------------- |
| Fossil Hybrid HR 42mm (Collider and others)    | DN1.0             |
| Fossil Hybrid HR 38mm (Monroe and others)      | IV0.0             |
| Fossil Gen 6 Hybrid 44mm/45mm (also Wellness)  | VA0.0             |
| Skagen Jorn 42mm Gen 6                         | VA0.0             |
| Skagen Jorn 38mm Gen 6                         | WA0.0             |

## Tested firmware versions

| Version            | From Fossil app | Comments  |
| ------------------ | --------------- | --------- |
| DN1.0.2.3r.prod.v8 | | Pre-installed, probably no real functionality except firmware update |
| DN1.0.2.12r.v2     | | |
| DN1.0.2.14r.v4     | | |
| DN1.0.2.16r.v9     | | |
| DN1.0.2.17r.v5     | 4.3.0 | |
| DN1.0.2.18r.v9     | 4.4.0 | |
| DN1.0.2.19r.v5     | | |
| DN1.0.2.19r.v7     | 4.5.0 | |
| DN1.0.2.20r.v5     | 4.6.0 | |
| DN1.0.2.21r.v1     | 4.6.0 | Upgrading to this version will invalidate your auth key, see notes |
| DN1.0.2.22r.v4     | 4.7.0 | |
| DN1.0.2.22r.v5     | 4.8.0 | |
| DN1.0.2.23r.v2     | 4.9.0 | |
| DN1.0.3.0r.v3      | 5.0.0 | |
| DN1.0.3.0r.v8      | 5.0.1 | |
| DN1.0.3.0r.v9      | 5.0.3 | |
| DN1.0.3.0r.v13     | 5.1.2 | |
| VA0.0.3.0r.v9      | | |
| VA0.0.3.0r.v13     | | |
| VA0.0.3.0r.v16     | | |
| VA0.0.3.0r.v17     | | |
| VA0.0.3.0r.v18     | | |
| VA0.0.3.0r.v19     | | |
| VA0.0.3.0r.v21     | | |

Notes:

* From DN1.0.2.18 up, more and more watch functions have moved from the firmware to watch apps. They can be uploaded with Gadgetbridge but we cannot re-distribute them. To make sure the watch is in fully working condition, upgrade it using the latest official Fossil app before switching to Gadgetbridge.

* It's possible to switch back and forth between the original app and Gadgetbridge if required for some reason. However the watches' secret key may change if you re-pair it with the official app, so you may need to repeat the key extraction process described below after switching back and forth.
