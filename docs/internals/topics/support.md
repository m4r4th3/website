---
title: Supporting a new gadget
---

# Supporting a new gadget

In theory we want to support all good and wide-spread gadgets.

In practice, someone needs to have the time and the information to implement support for it. If the communication protocol is open, or at least documented somewhere, adding support is rather easy. Ideally, gadgets would even use standard Bluetooth services that we could support in a generic way, with few device specific additions.

We do encourage and support the implementation for new gadgets.

When requesting support for a new device, please [use this issue template](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/new?template=.gitea%2fISSUE_TEMPLATE%2fdevice_request.md){: target="_blank" }. If you found documentation or even a specification about the protocol, please reference it. Such documentation very much increases the probability of supporting the device in question with Gadgetbridge.

## For hardware or firmware makers

If you are looking for an existing "standard Gadgetbridge protocol", to be used inside your gadget, it doesn't exist. For some inspiration, you can look for example at the implementation of [Bangle.js](https://codeberg.org/Freeyourgadget/Gadgetbridge/src/branch/master/app/src/main/java/nodomain/freeyourgadget/gadgetbridge/service/devices/banglejs/BangleJSDeviceSupport.java){: target="_blank" }. Even better, first study the official [Bluetooth GATT services](https://www.bluetooth.com/specifications/gatt/services/){: target="_blank" } specification. Some of these services are automatically detected by phones and other devices, and might not require a companion app, for example in order to display things like battery status or push music metadata.

---

If you want to learn how to add support for a new gadget into Gadgetbridge, continue from here:

[:material-arrow-right: New gadget tutorial](../development/new-gadget.md){: .gb-button }
