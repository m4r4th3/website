---
title: Thermometers
icon: material/thermometer
search:
  exclude: true
---

# Thermometers

This category contains thermometers.

{{ file_listing() }}
