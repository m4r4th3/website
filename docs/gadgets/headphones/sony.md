---
title: Sony
---

# Sony

??? note "List of features supported by Gadgetbridge"

    These gadget features are supported by Gadgetbridge and apply to all Sony headphone models included in this page. Note that actual available features per device depends on the device capabilities.

    * Ambient Sound Control
        * Off
        * Noise Cancelling
        * Wind Reduction
        * Ambient Sound
            * Volume level
            * Focus on Voice
    * Noise Canceling Optimizer
        * Trigger
        * Read atmospheric pressure
    * Sound Position Control
    * Surround Presets
    * Equalizer
        * Presets
        * Custom Presets 1/2 (Bands + Bass)
        * Manual Preset (Bands + Bass)
    * Audio Upsampling (DSEE HX/Extreme)
    * Touch sensor control panel
    * Button modes (left/right touch sensors)
    * Ambient Sound Control Button Mode
    * Manual Power Off from Gadgetbridge
    * Automatic Power Off
    * Pause when taken off
    * Notification & Voice Guide On/Off
    * Quick Access (double / triple tap)
    * Speak-to-chat
    * Read information from device
        * All User Settings
        * Firmware version
        * Audio Codec
        * Battery level Battery level (Single battery, dual battery, case)
    * Disable equalizer / sound position / surround mode when not in SBC codec

## LinkBuds S {{ device("sony_linkbuds_s") }}

Tested with firmware version 2.0.2.

See "List of features supported by Gadgetbridge" above to see all supported features common to Sony headphones.

Missing features:

* Multi-point Bluetooth, see {{ 2990|issue(749655) }}
* Adaptive noise cancelling
* Power off from app (that power button in the bar)

## WF-1000XM3 {{ device("sony_wf-1000xm3") }}

Tested with firmware version 3.1.2.

See "List of features supported by Gadgetbridge" above to see all supported features common to Sony headphones.

Missing features:

* Trigger Google Assistant / Alexa
* Notification & Voice Guide Language (needs the language files to be uploaded to the headphones on change)
* Firmware updates

## WF-1000XM4 {{ device("sony_wf-1000xm4") }}

Tested with firmware version 1.4.2.

See "List of features supported by Gadgetbridge" above to see all supported features common to Sony headphones.

Missing features are unknown.

## WF-1000XM5 {{ device("sony_wf-1000xm5") }}

See {{ 3341|issue }} about support of the gadget.

Missing features:

* Ambient Sound Control off setting
* Volume control from earbuds

## WF-SP800N {{ device("sony_wf-sp800n") }}

Tested with firmware version 1.1.0.

See "List of features supported by Gadgetbridge" above to see all supported features common to Sony headphones.

Missing features:

* Trigger Google Assistant / Alexa
* Notification & Voice Guide Language (needs the language files to be uploaded to the headphones on change)
* Firmware updates

## WH-1000XM2 {{ device("sony_wh-1000xm2") }}

Tested with firmware version 4.5.2.

See "List of features supported by Gadgetbridge" above to see all supported features common to Sony headphones.

Missing features are unknown.

## WH-1000XM3 {{ device("sony_wh-1000xm3") }}

Tested with firmware version 4.5.2.

See "List of features supported by Gadgetbridge" above to see all supported features common to Sony headphones.

Missing features:

* Sound Quality Mode (SBC Codec / HD codecs)
* Function of NC/AMBIENT button
* Notification & Voice Guide Language (needs the language files to be uploaded to the headphones on change)
* Firmware updates

## WH-1000XM4 {{ device("sony_wh-1000xm4") }}

Tested with firmware version 2.5.0, but not extensively tested.

See "List of features supported by Gadgetbridge" above to see all supported features common to Sony headphones.

Missing features:

* Power Off from app
* Function of "CUSTOM" button
* Connect to two devices simultaneously
* Speak-to-cheat settings (only enable/disable implemented)
* Notification & Voice Guide Language (needs the language files to be uploaded to the headphones on change)
* Firmware updates

## WH-1000XM5 {{ device("sony_wh-1000xm5") }}

Support was added to this gadget with {{ 2969|issue }}.

See "List of features supported by Gadgetbridge" above to see all supported features common to Sony headphones.

Tested with firmware version 1.1.3.

Missing features:

* Multi-point Bluetooth
* Adaptive noise cancelling
* Power off from app (that power button in the bar)
* Audio Upsampling
* Ambient sound control button mode
* Touch sensor enable/disable
* React to spotify trigger
* Voice assistant

## WI-SP600N {{ device("sony_wi-sp600n") }}

Support was added to this gadget with {{ 3522|pull }}.