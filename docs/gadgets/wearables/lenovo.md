---
title: Lenovo
---

# Lenovo

<!-- TODO: https://codeberg.org/mamutcho/Gadgetbridge/wiki#user-content-supported-watches-and-status -->

## Watch 9 {{ device("lenovo_watch_9") }}

## Watch X {{ device("lenovo_watch_x") }}

## Watch X Plus {{ device("lenovo_watch_x_plus") }}
