---
title: Wearables
icon: material/watch
search:
  exclude: true
---

# Wearables

This category contains wearables like smart watches.

{{ file_listing() }}
