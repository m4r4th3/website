---
title: Pine64
---

# Pine64

## PineTime {{ device("pine64_pinetime") }}

At this point, [PineTime](https://www.pine64.org/pinetime/){: target="_blank" } by [Pine64](https://www.pine64.org/){: target="_blank" } seems to have many developmental versions of firmware and the communication between watch/mobile device is not streamlined. Adding support for many versions of PineTime-foo and PineTime-bar are possible, but long term maintenance will be an issue. Merging these efforts would be best.

Initial PineTime support related conversation is in {{ 1824|issue }}.

Wasp-OS is a firmware for smart watches that are based on the nRF52 family of microcontrollers, and especially for hacker friendly watches such as the Pine64 PineTime. See more details [here.](https://wasp-os.readthedocs.io/en/latest/README.html){: target="_blank" }

We have added initial support for [InfiniTime](https://github.com/JF002/Pinetime){: target="_blank" }. It supports time setting and observing hardware and software version.

Supported features by Gadgetbridge:

* Steps (since InfiniTime 1.7)
    * InfiniTime advertise "steps" info when the PineTime screen is ON (and a bit after that). Hence:
        * To not loose your latest steps you should unlock the PineTime screen before the end of the day and also in the early morning.
        * When the PineTime screen is ON and you are moving, PineTime will send "steps" count every about 2-10 seconds, and Gadgetbridge may start to treat this data as an Activity (and also displaying it in Activity charts). That data and charts will not be accurate: you should wait for [Health/Fitness data storage and expose to companion app](https://github.com/InfiniTimeOrg/InfiniTime/projects/4){: target="_blank" } project to be implemented on the PineTime side. and meanwhile, in Gadgetbridge open "Device specific settings" and change/uncheck option in "Charts tabs" and "Activity info on device card" to leave only Steps data.
* Calls (accept/reject)
* Music
* Battery level
* Find device
* Transliteration
* Manual heart rate measurements

Note that InfiniTime currently does not support sleep tracking.
