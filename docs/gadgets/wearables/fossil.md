---
title: Fossil
---

# Fossil

All e-ink hybrid watches currently known from Fossil, Skagen and Citizen are supported. Note that the Gen 6 OLED smartwatches **are not supported.**

<div class="gb-figure-container" markdown="span" style="gap: 10px">
    <div class="gb-figure" markdown="span">
        ![](../../assets/static/screenshots/gadget/fossil_hybrid_hr_1.png){: height="180" style="height: 180px;" }
    </div><div class="gb-figure" markdown="span">
        ![](../../assets/static/screenshots/gadget/fossil_hybrid_hr_2.png){: height="180" style="height: 180px;" }
    </div><div class="gb-figure" markdown="span">
        ![](../../assets/static/screenshots/gadget/fossil_hybrid_hr_3.png){: height="180" style="height: 180px;" }
    </div><div class="gb-figure" markdown="span">
        ![](../../assets/static/screenshots/gadget/fossil_hybrid_hr_4.png){: height="180" style="height: 180px;" }
    </div>
</div>

<!-- TODO: https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/Fossil-Hybrid-HR -->

## Hybrid HR {{ device("fossil_hybrid_hr") }}

Includes 42mm and 38mm versions.

The Fossil Hybrid HR is an ePaper Hybrid smartwatch with real hands on a round ePaper display with over two weeks of battery. The same device is also sold with slightly different cases under the Skagen brand. Both the original generation and the Gen 6 devices are supported.

In the Hybrid HR settings on the watch, there is an option to mirror the "Do Not Disturb mode" on the phone. For this feature to work as expected, notifications will have to be sent to the watch regardless of the active Do Not Disturb mode on the phone. To achieve this, Gadgetbridge's global Do Not Disturb option in Notification Settings must be disabled. If this setting is enabled, all notifications that are suppressed by the phone's Do Not Disturb setting will be ignored completely.

### Supported features by Gadgetbridge

* Time synchronization
* Hands calibration
* Steps
* Heart rate
* GPS workouts (needs OpenTracks 3.29.0 or later installed)
* Phone notifications
* Call accept/decline
* Dismiss call with SMS
* Weather
* Physical buttons
* Music info and control
* Alarms (last alarm slot is used for quick alarm set from the widget)
* Watchface creation (watchfaces on firmware DN1.0.2.20r and up are only supported by Gadgetbridge 0.59.0 and up)
* Custom apps, menus and widgets (custom menu requires extra companion app, see below)

### Unofficial watch apps

* `navigationApp`
    * Displays navigation instructions from OsmAnd(+) and Google Maps. Distributed with Gadgetbridge, install or update it in the App Manager.
* [`snakeApp`](https://github.com/dakhnod/Fossil-HR-SDK){: target="_blank" }
    * Simple snake game
* [`timerApp`](https://github.com/dakhnod/Fossil-HR-Timer){: target="_blank" }
    * Integrated timer, stopwatch and alarm app. *Warning: replaces official Timer app, make sure to download and backup it first.*
* [`timerApp`](https://github.com/gjkrediet/Fossil_Timer){: target="_blank" }
    * Another timer app, aiming to be simpler and more user friendly. *Warning: replaces official Timer app, make sure to download and backup it first.*
* [`weatherApp`](https://github.com/gjkrediet/Fossil_Dutch_Weather){: target="_blank" }
    * Displays weather from Dutch weather organizations KNMI and Buienradar. *Warning: replaces official Weather app, make sure to download and backup it first*.

Note that unofficial apps that don't replace an official app cannot be started from the official `launcherApp` (menu). They can only be started from the custom menu or from the App Manager in Gadgetbridge.

### Missing features

* SpO2 measurements (WIP: see also {{ 2928|pull }})
* Sleep tracking (not tracked by watch but calculated by algorithm in official app)
* Alexa commands (initial support started, no progress lately)
* Google Fit integration (Gadgetbridge has no internet permissions)

For information about configuring the custom menu and automating with intents, see [Fossil specific intents](../../internals/automations/intents-gadget.md#fossil).

??? note "Firmware support"
    Firmware versions 2.19r and lower are probably still supported but unmaintained. If your watch is running such an old version, please consider updating it with the official vendor app.

    More information under [Internals - Fossil Hybrid HR](../../internals/specifics/fossil-hybrid.md).


## Gen 6 Hybrid {{ device("fossil_gen_6_hybrid") }}

Includes 42mm and 45mm versions.

The gen 6 hybrids are largely the same as the original Hybrid HR (gen 1) devices, but there are some significant improvements in the gen 6 hybrids:

* Better battery: 5 weeks battery life instead of 2 weeks
* Faster SoC: apps and user actions are faster
* More memory: the watch is more stable
* SpO2 measuring: not supported by Gadgetbridge yet
* Better heart rate sensor

Other details under [Hybrid HR](#hybrid-hr) also apply to this gadget.

<!-- TODO: Maybe move Skagen to its dedicated page? -->

## Skagen Jorn Gen 6 {{ device("skagen_jorn_gen_6") }}

Includes 42mm and 38mm versions.

Given details under [Gen 6 Hybrid](#gen-6-hybrid) also apply to this gadget.

## Q Hybrid {{ device("fossil_q_hybrid") }}

The Q Hybrid watches (those without eInk display) are probably still supported, but unmaintained.