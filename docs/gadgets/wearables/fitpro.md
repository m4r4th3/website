---
title: FitPro
---

# FitPro

This title covers all devices that use the FitPro app. Every device using the FitPro app should naturally work with Gadgetbridge as long as the software version is the same, but model names still need to be added to Gadgetbridge separately.

If you have a device that works with the FitPro app and Gadgetbridge detects it as "Not supported", you can try using the [Pairing unsupported gadgets](../../basics/pairing/index.md#pairing-unsupported-gadgets) option and select FitPro as the device type. Once you have paired your device, you can try to see if the features work and then let us know so that we can add the name of the device model to the list.

Overall, the FitPro devices are not bad for notifications, weather and occasional music control. The screen size isn't "full screen" as claimed and only has one button. Additionally, it doesn't have a touchscreen, so menu control requires short and long taps. The pedometer is not very reliable as it counts extra steps massively. The same can be said for the sleep and heart data - the measurements are super... fun. The battery seems to last between three and five days.

??? note "Additional pairing information"
    When pairing and connecting, is always seems important to press the button on the gadget to "wake it up" otherwise this can take a long time.

    Pairing with the gadget does not use any key and does not requre user confirmation on the gadget. Besided this, there is no protection and no secret/random generated code that would be used for subsequent connections, meaning that anyone at anytime (when the gadget is not connected to a phone) can connect to the gadget and fetch data. During the "Connecting new device" in Gadgetbridge, the user is asked by Gadgetbridge whether to "pair" or not. This question is related Android pairing. It is ussually better to select the "Pair" as it allows Gadgetbridge to re-connect in a better way, but during them implementation to Gadgetbridge, one of my devices did not work when it was paired... so i have decided to let the user to choose for now, perhaps over time this can be confirmed and set to "Pair" without asking.

    The "reconnect" parameter in the device coordinator is set to true, to try to connect to the gadget in situations that for example notification has arrived and the gadget is not connected. Overall, the bluetooth range of the gadget is quite small so disconnections are frequent.

<!-- TODO: https://codeberg.org/Freeyourgadget/Gadgetbridge/wiki/FitPro -->

## M6 {{ device("fitpro_m6") }}

Supported features by Gadgetbridge:

* Discovery, pairing and initialization is implemented, of course
* Set date and time
* Get device info and battery info. On the fly battery notification doesn't seem to be supported by the band. Also charging indication is not available from the band.
* Messages, calls and app notifications with some icons. It seems to support UTF characters.
* Find band, find phone. The "find band" vibration is very short, but if you first request heart rate measurement and immediately after you use the "Find lost device", the band will keep vibrating indefinitely. You can stop it with the "Found it".
* Reset (resets settings and steps) and unbind (same as reset plus says "unbinding") band (via Gadgetbridge Debug reboot and reset)
* Alarms - 8 slots. Quick alarm settings from the [Widget](../../basics/topics/widgets.md) is supported and overwrites the alarm slot number 8.
* Weather with icons
* Media button events, mapped to "previous", "pause" and "next" music controls.
* Camera buttons events are received but not handled further. In the future, these could be mapped to some custom actions, like take a photo, if we have support for it... This type of control seems to allow to prepare for a shot and then it fires with a wrist turn.
* Heart rate fetching - fetches heart rate, pressure and SpO2. These values are just a joke... but they do get stored to the database. Only HR can be seen in the charts.
<!-- removed - Continuous heart rate - this setting doesn't seem to work on any of my bands but worked on a device with very similar protocol, so i kept this in. -->
* Fetch "day summary" step/distance/calories statistics (this is not per minute data) - is implemented in the code but the data is not used anywhere.
* Steps counting - the band provides steps in 5 minute chunks. This means that often after data fetching, few of the most recent data "seems missing" (the last few, maximum up to five minutes) The original app shows the "day summary steps" to the user to get around that. Gadgetbridge calculates real steps from the real provided activity data. Steps, distance and calories are fetched, only steps are presented in the charts.
* Sleep data - the band provides sleep (light sleep, deep sleep, awake) in 15 minute chunks.

Missing features:

* Software update
* Watchface management
* Remote camera trigger

## M4 {{ device("fitpro_m4") }}

Given details under [M6](#m6) also apply to this gadget.

## LH716 {{ device("fitpro_lh716") }}

Given details under [M6](#m6) also apply to this gadget.

## Sunset 6 {{ device("fitpro_sunset_6") }}

Given details under [M6](#m6) also apply to this gadget.

## Watch7 {{ device("fitpro_watch_7") }}

Given details under [M6](#m6) also apply to this gadget.

## Fit1900 {{ device("fitpro_fit_1900") }}

Given details under [M6](#m6) also apply to this gadget.

## ColaCao 2021 {{ device("colacao_2021") }}

The ColaCao watches use the same FitPro protocol, but do not have an heart rate sensor and crash if it is attempted to be used, so heart rate support is disabled in Gadgetbridge.

Reported to be working with the FitPro implementation in {{ 2955 | issue }}.

Given details under [M6](#m6) also apply to this gadget, except for any heart rate support.

## ColaCao 2023 {{ device("colacao_2023") }}

Reported to be working with the FitPro implementation in {{ 3455 | issue }}.

Given details under [ColaCao 2021](#colacao-2021) also apply to this gadget.
