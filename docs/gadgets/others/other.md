---
title: Others & unbranded
---

# Others & unbranded

## VESC {{ device("other_vesc") }}

BLDC controller VESC.

## UM25 {{ device("other_um25") }}

USB voltage meter.

## Vibratissimo {{ device("vibratissimo_any") }}

A... massage gadget... as you can probably call by its name.

Support for this gadget was added with {{ "8ba7bc7353"|commit }}.

To be real, support was added as a joke ({{ 1235|issue(41670) }}) in 2016, and it is not to be taken seriously. It was added after [the lawsuit](https://arstechnica.com/wp-content/uploads/2016/09/vibratorsuit.pdf){: target="_blank" } about [another vibrator collecting information without user consent.](https://arstechnica.com/tech-policy/2016/12/maker-of-internet-of-things-connected-vibrator-will-settle-privacy-suit/){: target="_blank" }
